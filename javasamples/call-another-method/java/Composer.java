package namek;
public class Composer{
	
  private String fname;
  
  private String lname;
  
  public void SetFname(String name){
    this.fname = name;
  }
  
  public void SetLname(String name){
    this.lname = name;
  }  

  public String getName(){
	return fname + " " + lname;
  }

}