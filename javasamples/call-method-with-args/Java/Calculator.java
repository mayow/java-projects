import java.util.Scanner; 

public class Calculator {

    public static void main(String[] args) {
        Calculator calc = new Calculator();
        calc.start();
    }
    
    public void start(){
        Scanner scan = new Scanner(System.in);
        double num1 = scan.nextDouble();
        double num2 = scan.nextDouble();  
        print(Double.toString(sum(num1,num2)));
    }    
    
    public static void print (String str){
        System.out.println(str);
    }
    
    public static double sum (double x, double y){
        return x + y;
    }
    
}