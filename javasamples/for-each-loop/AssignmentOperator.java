//https://www.programiz.com/java-programming/enhanced-for-loop
class AssignmentOperator {
   public static void main(String[] args) {
      
      char[] vowels = {'a', 'e', 'i', 'o', 'u'};
      
      for (char item: vowels) {
         System.out.println(item);
      }
   }
}