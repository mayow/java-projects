

public class ListForLoop {
	
	public static void main(String[] args){
		int[] list = {1,2,3,4,5,6,7,8,9};
		int addedtotal = sumoflist(list);
		System.out.println("total sum of all number is: " + addedtotal);
	}
	
	public static int sumoflist(int[] list){
		int total = 0;
		for(int val : list) {
			total += val;
		}
		return total;
	}
	
}