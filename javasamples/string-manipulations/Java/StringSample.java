

public class StringSample {
    
    public static void main(String[] args){
        String s_string = "This is a string";
            System.out.println(s_string);
        s_string = "This " + "is " + "a " + "concatenated " + "string";
            System.out.println(s_string);
        s_string = "This " + "is " + "a " + "concatenated " + "string";
            System.out.println(s_string.substring(0,20));       
    }
    
}