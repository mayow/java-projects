import java.io.*;

public class UserInput3 {

	public static void main(String[]args){
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String name = "";
		System.out.println("Anata no onamae wa?:");
		try{ 
		   name = reader.readLine(); 
		} catch(IOException e) { 
		  System.out.print("Error");
		} 
        System.out.println("konichiwa! " + name);
	}
	
}